import { combineReducers, applyMiddleware, createStore } from "redux";
import { logger } from "redux-logger";
import { RootReducer } from "./reducers";


const reducers = combineReducers({
  rootReducer: RootReducer
});

const middleware = applyMiddleware(logger);

let store = createStore(reducers, middleware);

export default store;
